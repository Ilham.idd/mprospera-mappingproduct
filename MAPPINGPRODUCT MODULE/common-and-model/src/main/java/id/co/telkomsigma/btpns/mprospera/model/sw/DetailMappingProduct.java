package id.co.telkomsigma.btpns.mprospera.model.sw;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.*;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_MAPPING_PRODUCT")
public class DetailMappingProduct extends GenericModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long mappingProductId;
	private Long productIdMapping;
	private String status;
	private String jenisMMS;
	private String ketentuan;
	private String tujuanPembiayaan;
	private String regularPiloting;
	private int plafondMin;
	private int plafonMax;
	private Date updateDate;

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public Long getMappingProductId() {
		return mappingProductId;
	}
	public void setMappingProductId(Long mappingProductId) {
		this.mappingProductId = mappingProductId;
	}
	
	@Column(name = "product_id")
	public Long getProductIdMapping() {
		return productIdMapping;
	}
	public void setProductIdMapping(Long productIdMapping) {
		this.productIdMapping = productIdMapping;
	}
	
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "jenis_MMS")
	public String getJenisMMS() {
		return jenisMMS;
	}
	public void setJenisMMS(String jenisMMS) {
		this.jenisMMS = jenisMMS;
	}
	
	@Column(name = "ketentuan")
	public String getKetentuan() {
		return ketentuan;
	}
	public void setKetentuan(String ketentuan) {
		this.ketentuan = ketentuan;
	}
	
	@Column(name = "tujuan_pembiayaan")
	public String getTujuanPembiayaan() {
		return tujuanPembiayaan;
	}
	public void setTujuanPembiayaan(String tujuanPembiayaan) {
		this.tujuanPembiayaan = tujuanPembiayaan;
	}
	
	@Column(name = "reguler_piloting")
	public String getRegularPiloting() {
		return regularPiloting;
	}
	public void setRegularPiloting(String regularPiloting) {
		this.regularPiloting = regularPiloting;
	}
	
	@Column(name = "plafon_min")
	public int getPlafondMin() {
		return plafondMin;
	}
	public void setPlafondMin(int plafondMin) {
		this.plafondMin = plafondMin;
	}
	
	@Column(name = "plafon_max")
	public int getPlafonMax() {
		return plafonMax;
	}
	public void setPlafonMax(int plafonMax) {
		this.plafonMax = plafonMax;
	}
	
	@Column(name = "update_date")
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	
}
