package id.co.telkomsigma.btpns.mprospera.model.sw;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "T_LAON_PRODUCT_MAPPING")
public class LoanProductMapping extends GenericModel{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Long mappingId;
    private LoanProduct productId;
    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getMappingId() {
        return mappingId;
    }

    public void setMappingId(Long mappingId) {
        this.mappingId = mappingId;
    }
    
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = LoanProduct.class)
    @JoinColumn(name = "product_id", referencedColumnName = "id", nullable = true)
    public LoanProduct getProductId() {
        return productId;
    }

    public void setProductId(LoanProduct productId) {
        this.productId = productId;
    }

}
