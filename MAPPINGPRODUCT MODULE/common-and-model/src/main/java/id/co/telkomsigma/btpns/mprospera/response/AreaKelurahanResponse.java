package id.co.telkomsigma.btpns.mprospera.response;

public class AreaKelurahanResponse {

    private String areaName;
    private String areaId;
    private String parentAreaId;
    private String postalCode;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getParentAreaId() {
        return parentAreaId;
    }

    public void setParentAreaId(String parentAreaId) {
        this.parentAreaId = parentAreaId;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        if (postalCode != null) {
            this.postalCode = postalCode;
        } else {
            this.postalCode = "";
        }
    }

}