package id.co.telkomsigma.btpns.mprospera.response;

/**
 * Created by Dzulfiqar on 12/04/2017.
 */
public class UserBwmpList {

    private String userBwmp;
    private String role;
    private String name;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserBwmp() {

        return userBwmp;
    }

    public void setUserBwmp(String userBwmp) {
        this.userBwmp = userBwmp;
    }

}