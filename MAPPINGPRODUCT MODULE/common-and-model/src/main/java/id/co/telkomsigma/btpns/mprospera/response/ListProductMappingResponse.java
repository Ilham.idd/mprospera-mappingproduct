package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class ListProductMappingResponse extends BaseResponse {
	
    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private String Version;
    private List<ProductMappingResponse> productMappingResponses;
    
    
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	public String getCurrentTotal() {
		return currentTotal;
	}
	public void setCurrentTotal(String currentTotal) {
		this.currentTotal = currentTotal;
	}
	public String getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(String totalPage) {
		this.totalPage = totalPage;
	}
	public String getVersion() {
		return Version;
	}
	public void setVersion(String version) {
		Version = version;
	}
	
	public List<ProductMappingResponse> getProductMappingResponses() {
		if(productMappingResponses ==null)
			productMappingResponses	= new ArrayList<>();
		return productMappingResponses;
	}
	public void setProductMappingResponses(List<ProductMappingResponse> productMappingResponses) {
		this.productMappingResponses = productMappingResponses;
	}
	
	

   
}
