package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@SuppressWarnings("all")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ProductMappingResponse{
    private Long productId;
    private String prosperaId;
    private String productName;
    private Integer tenor;
    private Integer installmentCount;
    private String loanType;
    private String installmentFreqTime;
    private Integer installmentFreqCount;
    private String status;
    private BigDecimal margin;
    private BigDecimal productRate;
	private String productCode;
    private List<DetailProductMappingResponse> listDetailProductMapping;
    

	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProsperaId() {
		return prosperaId;
	}
	public void setProsperaId(String prosperaId) {
		this.prosperaId = prosperaId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getTenor() {
		return tenor;
	}
	public void setTenor(Integer tenor) {
		this.tenor = tenor;
	}
	public Integer getInstallmentCount() {
		return installmentCount;
	}
	public void setInstallmentCount(Integer installmentCount) {
		this.installmentCount = installmentCount;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public String getInstallmentFreqTime() {
		return installmentFreqTime;
	}
	public void setInstallmentFreqTime(String installmentFreqTime) {
		this.installmentFreqTime = installmentFreqTime;
	}
	public Integer getInstallmentFreqCount() {
		return installmentFreqCount;
	}
	public void setInstallmentFreqCount(Integer installmentFreqCount) {
		this.installmentFreqCount = installmentFreqCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getMargin() {
		return margin;
	}
	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}
	public BigDecimal getProductRate() {
		return productRate;
	}
	public void setProductRate(BigDecimal productRate) {
		this.productRate = productRate;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public List<DetailProductMappingResponse> getListDetailProductMapping()  {
		if(listDetailProductMapping == null) {
			listDetailProductMapping = new ArrayList<>();
		}
		return listDetailProductMapping ;
	}
	public void setListDetailProductMapping(List<DetailProductMappingResponse> listDetailProductMapping) {
		this.listDetailProductMapping = listDetailProductMapping;
	}
    
	    
	    
}
