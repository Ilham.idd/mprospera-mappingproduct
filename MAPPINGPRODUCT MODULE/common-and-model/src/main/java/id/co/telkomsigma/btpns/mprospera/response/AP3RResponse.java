package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class AP3RResponse extends BaseResponse {

    private String ap3rId;
    private String localId;
    private String status;
    private List<AP3RListResponse> ap3rList;
    private String createdAt;
    private String updatedAt;

    public String getAp3rId() {
        return ap3rId;
    }

    public void setAp3rId(String ap3rId) {
        this.ap3rId = ap3rId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<AP3RListResponse> getAp3rList() {
        return ap3rList;
    }

    public void setAp3rList(List<AP3RListResponse> ap3rList) {
        this.ap3rList = ap3rList;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "AP3RResponse [ap3rId=" + ap3rId + ", localId=" + localId + ", status=" + status + ", ap3rList="
                + ap3rList + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", getResponseCode()="
                + getResponseCode() + ", getResponseMessage()=" + getResponseMessage() + "]";
    }

}