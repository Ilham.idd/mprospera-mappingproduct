package id.co.telkomsigma.btpns.mprospera.response;

import java.util.Arrays;

/**
 * Created by Dzulfiqar on 28/04/2017.
 */
public class PhotoResponse extends BaseResponse {

    private byte[] photo;

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "PhotoResponse{" +
                "photo=" + Arrays.toString(photo) +
                '}';
    }

}