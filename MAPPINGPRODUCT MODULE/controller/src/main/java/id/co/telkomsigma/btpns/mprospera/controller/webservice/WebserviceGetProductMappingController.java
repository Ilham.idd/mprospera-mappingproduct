package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;

import java.io.BufferedInputStream;
import java.util.Date;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.sw.DetailMappingProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.response.DetailProductMappingResponse;
import id.co.telkomsigma.btpns.mprospera.response.ListProductMappingResponse;
import id.co.telkomsigma.btpns.mprospera.response.ProductMappingResponse;
import id.co.telkomsigma.btpns.mprospera.service.ProductMappingService;



@Controller("webserviceGetProductMappingController")
public class WebserviceGetProductMappingController extends GenericController {
	
    @Autowired
    private ProductMappingService productMappingService;
    
    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_MAPPING_PRODUCT_VERSION,method = {RequestMethod.POST})
    public @ResponseBody String cekVersiFileMappingProduct(HttpServletRequest request,HttpServletResponse response,@PathVariable ("version") Long version) throws ParseException{
		log.info("Incoming Message For validating Version");
		Date date = new Date(version);
		log.info("VERSION_date : "+date);
		java.sql.Date dateSQL = new java.sql.Date(date.getTime());
		log.info("VERSION_dateSQL : "+dateSQL);
    	String mappingProductCekVersi = productMappingService.getVersionByUpdateDate(date,version);
    	log.info(mappingProductCekVersi.toString());
    	return mappingProductCekVersi;
    }
    
	@RequestMapping(value = WebGuiConstant.TERMINAL_GET_MAPPING_PRODUCT_REQUEST, method = {RequestMethod.GET})
    public void getMappingProductAll(final HttpServletRequest request, final HttpServletResponse response,
                                      @PathVariable("apkVersion") String filename)
//                                      @PathVariable("id") String id,
//                                      @RequestHeader(value = "transmissionDateAndTime", required = true) String transmissionDateAndTime,
//                                      @RequestHeader(value = "retrievalReferenceNumber", required = true) final String retrievalReferenceNumber,
//                                      @RequestHeader(value = "username", required = true) final String username,
//                                      @RequestHeader(value = "sessionKey", required = true) final String sessionKey,
//                                      @RequestHeader(value = "imei", required = true) final String imei)  
                                   throws IOException{
		
		final ListProductMappingResponse listProductMappingResponse = new ListProductMappingResponse();
		listProductMappingResponse.setResponseCode(WebGuiConstant.RC_SUCCESS);
		log.info("Validation Success, get Jenis Usaha Data");		
		List<LoanProduct> listproductMapping =  productMappingService.getAllLoanProductId();
		for(LoanProduct loanProdutMappingFromList : listproductMapping) {
			ProductMappingResponse productMapping = new ProductMappingResponse();
			productMapping.setProductId(loanProdutMappingFromList.getProductId());
			productMapping.setProductName(loanProdutMappingFromList.getProductName());
			productMapping.setLoanType(loanProdutMappingFromList.getLoanType());
			productMapping.setProductCode(loanProdutMappingFromList.getProductCode());
			productMapping.setTenor(loanProdutMappingFromList.getTenor());
			productMapping.setMargin(loanProdutMappingFromList.getMargin());
			
		//	final DetailProductMappingResponse listDetailProductMappingResponse = new DetailProductMappingResponse();
			List<DetailMappingProduct> listdDetailProductMapping = productMappingService.getLoanProductId(loanProdutMappingFromList.getProductId());
			
			
			for(DetailMappingProduct detailMappingProductFromList : listdDetailProductMapping) {
				DetailProductMappingResponse detailProductMapping = new DetailProductMappingResponse();
				if(detailMappingProductFromList.getMappingProductId() != loanProdutMappingFromList.getProductId()) {
					detailProductMapping.setProductIdMapping(detailMappingProductFromList.getProductIdMapping());
					detailProductMapping.setPlafondMin(detailMappingProductFromList.getPlafondMin());
					detailProductMapping.setPlafonMax(detailMappingProductFromList.getPlafonMax());
					detailProductMapping.setJenisMMS(detailMappingProductFromList.getJenisMMS());
					detailProductMapping.setStatus(detailMappingProductFromList.getStatus());
					detailProductMapping.setKetentuan(detailMappingProductFromList.getKetentuan());
					detailProductMapping.setTujuanPembiayaan(detailMappingProductFromList.getTujuanPembiayaan());
					detailProductMapping.setRegularPiloting(detailMappingProductFromList.getRegularPiloting());
				}else {
				detailProductMapping.setProductIdMapping(null);
				detailProductMapping.setPlafondMin(0);
				detailProductMapping.setPlafonMax(0);
				detailProductMapping.setJenisMMS("");
				detailProductMapping.setStatus("");
				detailProductMapping.setKetentuan("");
				detailProductMapping.setTujuanPembiayaan("");
				detailProductMapping.setRegularPiloting("");
				}
				productMapping.getListDetailProductMapping().add(detailProductMapping);
			}
			
			listProductMappingResponse.getProductMappingResponses().add(productMapping);
		}
		log.info("GET data mapping product Success");
		
		ObjectMapper mapper = new ObjectMapper();
		listProductMappingResponse.setVersion("V1.1");
		File file = new File("usaha.json");
		try {    
			// Serialize Java object info JSON file.
			mapper.writeValue(file, listProductMappingResponse);
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		response.setContentType("application/json");

		response.setHeader("Content-Disposition", "attachment; filename=productMapping.json");

		response.setContentLength((int) file.length());

		InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

		FileCopyUtils.copy(inputStream, response.getOutputStream());
		
	}
	//Mapping Product per Id
	@RequestMapping(value = WebGuiConstant.TERMINAL_BUSINESS_MAPPING_PRODUCT_LINK_REQUEST, method = {RequestMethod.GET})
    public void getMappingProductId(final HttpServletRequest request, final HttpServletResponse response,
                                      @PathVariable("apkVersion") String filename,@PathVariable("id") Long id)throws IOException {
			
		final ListProductMappingResponse listProductMappingResponse = new ListProductMappingResponse();
		listProductMappingResponse.setResponseCode(WebGuiConstant.RC_SUCCESS);
		log.info("Validation Success, get Jenis Usaha Data");
		DetailMappingProduct lastUpdateDate = null;
		List<LoanProduct> listproductMapping =  productMappingService.getFindLoanByProductId(id);
		for(LoanProduct loanProdutMappingFromList : listproductMapping) {
			ProductMappingResponse productMapping = new ProductMappingResponse();
			productMapping.setProductId(loanProdutMappingFromList.getProductId());
			productMapping.setProductName(loanProdutMappingFromList.getProductName());
			productMapping.setLoanType(loanProdutMappingFromList.getLoanType());
			productMapping.setProductCode(loanProdutMappingFromList.getProductCode());
			productMapping.setTenor(loanProdutMappingFromList.getTenor());
			productMapping.setMargin(loanProdutMappingFromList.getMargin());
			
		//	final DetailProductMappingResponse listDetailProductMappingResponse = new DetailProductMappingResponse();
			List<DetailMappingProduct> listdDetailProductMapping = productMappingService.getLoanProductId(loanProdutMappingFromList.getProductId());
			if(!listdDetailProductMapping.isEmpty()) {
				lastUpdateDate = listdDetailProductMapping.get(listdDetailProductMapping.size()-1);
				log.info("lastUpdateDate : "+lastUpdateDate);
			}
			for(DetailMappingProduct detailMappingProductFromList : listdDetailProductMapping) {	
				DetailProductMappingResponse detailProductMapping = new DetailProductMappingResponse();
				if(detailMappingProductFromList.getMappingProductId() != loanProdutMappingFromList.getProductId()) {
					
					detailProductMapping.setProductIdMapping(detailMappingProductFromList.getProductIdMapping());
					detailProductMapping.setPlafondMin(detailMappingProductFromList.getPlafondMin());
					detailProductMapping.setPlafonMax(detailMappingProductFromList.getPlafonMax());
					detailProductMapping.setJenisMMS(detailMappingProductFromList.getJenisMMS());
					detailProductMapping.setStatus(detailMappingProductFromList.getStatus());
					detailProductMapping.setKetentuan(detailMappingProductFromList.getKetentuan());
					detailProductMapping.setTujuanPembiayaan(detailMappingProductFromList.getTujuanPembiayaan());
					detailProductMapping.setRegularPiloting(detailMappingProductFromList.getRegularPiloting());					
					detailProductMapping.setUpdateDate(lastUpdateDate.getUpdateDate());
				}else {
				detailProductMapping.setProductIdMapping(null);
				detailProductMapping.setPlafondMin(0);
				detailProductMapping.setPlafonMax(0);
				detailProductMapping.setJenisMMS("");
				detailProductMapping.setStatus("");
				detailProductMapping.setKetentuan("");
				detailProductMapping.setTujuanPembiayaan("");
				detailProductMapping.setRegularPiloting("");
				detailProductMapping.setUpdateDate(null);
				}
				productMapping.getListDetailProductMapping().add(detailProductMapping);
			}
			
			listProductMappingResponse.getProductMappingResponses().add(productMapping);
		}
		log.info("GET data mapping product Success");
		
		ObjectMapper mapper = new ObjectMapper();
		listProductMappingResponse.setVersion(lastUpdateDate.getUpdateDate().toString()+"_"+lastUpdateDate.getUpdateDate().getTime());
		File file = new File("mappingProduct.json");
		try {
			// Serialize Java object info JSON file.
			mapper.writeValue(file, listProductMappingResponse);
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		response.setContentType("application/json");

		response.setHeader("Content-Disposition", "attachment; filename=productMapping.json");

		response.setContentLength((int) file.length());

		InputStream inputStreamId = new BufferedInputStream(new FileInputStream(file));

		FileCopyUtils.copy(inputStreamId, response.getOutputStream());

	}
	
}
