package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.ListMMRequest;
import id.co.telkomsigma.btpns.mprospera.response.ListProductMappingResponse;
import id.co.telkomsigma.btpns.mprospera.response.ProductMappingResponse;
import id.co.telkomsigma.btpns.mprospera.service.ProductMappingService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.PercentageSynchronizer;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("productMappingController")
public class WebserviceProductMappingController extends GenericController{

    @Autowired
    private WSValidationService wsValidationService;
    
    @Autowired
    private ProductMappingService productMappingService;
    
    @Autowired
    private TerminalService terminalService;
    
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    
    @Autowired
    private TerminalActivityService terminalActivityService;
    
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd hh:mm:ss");
    JsonUtils jsonUtils = new JsonUtils();
    
    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_PRODUCT_MAPPING_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    ListProductMappingResponse listProductMapping(@RequestBody final ListMMRequest request,
                          @PathVariable("apkVersion") String apkVersion) {
    	
    	final String imei = request.getImei();
    	String countData = request.getGetCountData().toString();
    	final String page = request.getPage().toString();
    	final ListProductMappingResponse response = new  ListProductMappingResponse();
    	response.setResponseCode(WebGuiConstant.RC_SUCCESS);
    	try {
    		 log.info("listMM INCOMING MESSAGE : " + jsonUtils.toJson(request));
    		 String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                     request.getSessionKey(), apkVersion);
    		if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
    			 response.setResponseCode(validation);
                 String label = getMessage("webservice.rc.label." + response.getResponseCode());
                 response.setResponseMessage(label);
                 log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
    		}else {
    			 Timer timer = new Timer();
                 timer.start("[" + WebGuiConstant.TERMINAL_GET_PRODUCT_MAPPING_REQUEST + "] [productMappingService.getProductMapping]"); 
                 final Page<LoanProduct> lps = productMappingService.getProductMapping(page, countData, request.getUsername());
                 timer.stop();
    			 final List<ProductMappingResponse> productMappingResponses = new ArrayList<>();
    			 for(final LoanProduct lp : lps.getContent()) {
    				 ProductMappingResponse productMappingResponse = new ProductMappingResponse();
    				 productMappingResponse.setProductId(lp.getProductId());
    				 productMappingResponse.setProductName(lp.getProductName());
    				 
    				 productMappingResponses.add(productMappingResponse);
    			 }
                 Terminal terminal = terminalService.loadTerminalByImei(imei);
                 terminal.setMmProgress(PercentageSynchronizer.processSyncPercent(page,
                         String.valueOf(lps.getTotalPages()), terminal.getMmProgress()));
                 terminalService.updateTerminal(terminal);
                 
                 response.setCurrentTotal(String.valueOf(lps.getContent().size()));
                 response.setGrandTotal(String.valueOf(lps.getTotalElements()));
                 response.setTotalPage(String.valueOf(lps.getTotalPages()));
                 //response.setProductMappingList(productMappingResponses);
                 String label = getMessage("webservice.rc.label." + response.getResponseCode());
                 response.setResponseMessage(label);
                
    		}	  
		} catch (Exception e) {
			log.error("listProductMapping error: " + e.getMessage());
            e.printStackTrace();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("listProductMapping error: " + e.getMessage());
		} finally {
			try {
				 // insert MessageLogs
				log.info("Trying to CREATE Terminal Activity...");
				threadPoolTaskExecutor.execute(new Runnable() {
					@Override
					public void run() {
						 	Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
	                        TerminalActivity terminalActivity = new TerminalActivity();
	                        // Logging to terminal activity   
	                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
	                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_PRODUCTMAPPING);
	                        terminalActivity.setCreatedDate(new Date());
	                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
	                        terminalActivity.setSessionKey(request.getSessionKey());
	                        terminalActivity.setTerminal(terminal);
	                        terminalActivity.setUsername(request.getUsername().trim());
	                        List<MessageLogs> messageLogs = new ArrayList<>();
	                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
	                        log.info("TERMINAL ACTIVITY LOGGING SUCCESS...");
	                        log.info("Updating Terminal Activity");
					}
				});
				 log.info("listMM RESPONSE MESSAGE : " + jsonUtils.toJson(response));
			} catch (Exception e2) {
				 log.error("listMM ERROR CREATE terminalActivity on TERMINAL_GET_MM_REQUEST : " + e2.getMessage());
			}
			
			return response;
		}
  
    }

}
