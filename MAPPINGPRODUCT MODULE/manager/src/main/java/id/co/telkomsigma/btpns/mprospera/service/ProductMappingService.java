package id.co.telkomsigma.btpns.mprospera.service;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.manager.LoanProductMappingManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.DetailMappingProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

@Service("productMappingService")
public class ProductMappingService extends GenericService{
	
	 @Autowired
	 @Qualifier("loanProductMappingManager")
	private LoanProductMappingManager loanProductMappingManager;

	 
    public Integer countAll() {
	        return loanProductMappingManager.countAll();
	}
	   
	public Page<LoanProduct> getProductMapping(String page, String getCountData,String username)  throws ParseException{
		if (getCountData == null)
            page = null;
		
		 int pageNumber = Integer.parseInt(page) - 1;
		 
		 if (page == null || page.equals(""))
             return loanProductMappingManager.findAllProductMapping();
         else
             return loanProductMappingManager.findAllProductMappingPageable(new PageRequest(pageNumber, Integer.parseInt(getCountData)));

	}
	
	public List <DetailMappingProduct> getLoanProductId(Long productIdMapping) {
		return loanProductMappingManager.getLoanProductId(productIdMapping);
	}
	
	//All Mapping Product Json
	public List<LoanProduct> getAllLoanProductId(){
		return loanProductMappingManager.getAllLoanProductId();
	}
	
	//FindById
	public List<LoanProduct> getFindLoanByProductId(Long productId){
		return loanProductMappingManager.getFindLoanByProductId(productId);
	}
	
	//findByDate For Versi
	public String getVersionByUpdateDate(Date updateDate, Long version) throws ParseException {
		long timestime = 0;
		DetailMappingProduct detailMappingProduct = loanProductMappingManager.getVersionByUpdateDate(updateDate); 
		if(detailMappingProduct != null ) {
			log.info("detailMappingProduct : "+detailMappingProduct);
			
			timestime = detailMappingProduct.getUpdateDate().getTime();
			
			log.info("Timestime : " + timestime);
			
			DetailMappingProduct detailMappingProductTopOne = loanProductMappingManager.findTopOneByUpdateDateOrderByUpdateDateDesc(updateDate);
			log.info("detailMappingProductTopOne : " + detailMappingProductTopOne);
			if(detailMappingProductTopOne!=null) {
				long timestimeTopOne = detailMappingProductTopOne.getUpdateDate().getTime();
				
				log.info("detailMappingProductTopOne : "+timestimeTopOne);
				
				if(timestime != 0 && timestime < timestimeTopOne) {
					return WebGuiConstant.MAPPING_PRODUCT_STATUS_TRUE;
				}else {
					return WebGuiConstant.MAPPING_PRODUCT_STATUS_FALSE;
				}
			}else {
				return WebGuiConstant.MAPPING_PRODUCT_STATUS_FALSE;
			}

		}else {
			return WebGuiConstant.MAPPING_PRODUCT_STATUS_NOT_FOUND;
		}
	}

//	public List<DetailMappingProduct> findByProductIdMapping(LoanProduct productId){
//		return loanProductMappingManager.findByProductIdMapping(productId);
//	}
	
}
