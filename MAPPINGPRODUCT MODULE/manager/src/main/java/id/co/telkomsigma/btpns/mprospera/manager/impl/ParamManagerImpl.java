package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.ParamDao;
import id.co.telkomsigma.btpns.mprospera.manager.ParamManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("paramManager")
public class ParamManagerImpl implements ParamManager {

    @Autowired
    private ParamDao paramDao;

    @Override
    public List<SystemParameter> getAll() {
        return paramDao.findAll();
    }

    @Override
    public SystemParameter getParamByParamName(String paramName) {
        SystemParameter param = paramDao.findByParamName(paramName);
        if (param != null)
            param.getCreatedBy().setOfficeCode(null);
        return param;
    }

    @Override
    public void clearCache() {
    }

}