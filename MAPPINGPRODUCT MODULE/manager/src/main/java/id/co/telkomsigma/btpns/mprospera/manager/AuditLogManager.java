package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;

import java.util.List;


public interface AuditLogManager {

    List<AuditLog> getAll();

    AuditLog insertAuditLog(AuditLog auditLog);

}