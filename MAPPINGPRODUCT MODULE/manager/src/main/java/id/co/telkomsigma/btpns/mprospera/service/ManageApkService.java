package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.ManageApkManager;
import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("manageApkService")
public class ManageApkService extends GenericService {

    @Autowired
    private ManageApkManager manageApkManager;

    public List<ManageApk> getAll() {
        return manageApkManager.getAll();
    }

}