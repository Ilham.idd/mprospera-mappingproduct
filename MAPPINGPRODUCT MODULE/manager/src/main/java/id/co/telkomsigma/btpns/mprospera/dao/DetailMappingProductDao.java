package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.sw.DetailMappingProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;


public interface DetailMappingProductDao extends JpaRepository<DetailMappingProduct, Long> {

	//List<DetailMappingProduct> findByproductIdMapping(LoanProduct productId);
	
//    @Query("SELECT l FROM DetailMappingProduct l inner join LoanProduct k on  where l.productIdMapping = :productIdMapping")
//	//@Query("SELECT p FROM LoanProduct p INNER JOIN p.detailMappingProduct mp ON p.productId = mp.productIdMapping WHERE mp.productIdMapping = :productIdMapping")
//    List<DetailMappingProduct> getproductId(@Param("productIdMapping") Long productId);
//    
    List<DetailMappingProduct> findByproductIdMapping(Long productIdMapping);
    
    DetailMappingProduct getTopOneByupdateDate (Date updateDate);
    
    DetailMappingProduct findTopOneByUpdateDateGreaterThanOrderByUpdateDateDesc(Date detailMappingProductDao);
	
}
