package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.btpns.mprospera.model.sw.DetailMappingProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

public interface LoanProductMappingDao extends JpaRepository<LoanProduct, Long> {

	@Query("SELECT COUNT(p) FROM LoanProduct m")
    int countAll();
	
    @Query("SELECT p FROM LoanProduct p ORDER BY p.tenor ASC")
    Page<LoanProduct> findAll(Pageable pageable);
    
    Page<LoanProduct> findByProductId(List<String> kelIdList, Pageable pageRequest);
    
    @Query("SELECT l FROM LoanProduct l where l.productId = :productId")
    LoanProduct getproductId(@Param("productId") Long productId);
    
    List<LoanProduct> findByproductId(Long productId);
    
	

}
