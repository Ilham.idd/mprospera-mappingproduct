package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.PDKManager;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("pdkService")
public class PDKService extends GenericService {

    @Autowired
    @Qualifier("pdkManager")
    private PDKManager pdkManager;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

    public Page<PelatihanDasarKeanggotaan> getPDK(List<String> userList, String page, String getCountData,
                                                  String startDate, String endDate) throws ParseException {
        if (getCountData == null)
            page = null;
        else if (page == null || "".equals(page))
            page = "1";
        if ((startDate == null || startDate.equals("")) && endDate != null)
            startDate = endDate;
        if (endDate == null || endDate.equals("") && startDate != null)
            endDate = startDate;
        if (startDate == null || startDate.equals(""))
            if (page == null || page.equals(""))
                return pdkManager.findAll(userList);
            else
                return pdkManager.findAllPageable(userList,
                        new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)));
        else if (page == null || page.equals(""))
            return pdkManager.findByCreatedDate(userList, formatter.parse(startDate), formatter.parse(endDate));
        else
            return pdkManager.findByCreatedDatePageable(userList, formatter.parse(startDate), formatter.parse(endDate),
                    new PageRequest(Integer.parseInt(page) - 1, Integer.parseInt(getCountData)));
    }

    public Integer countAll() {
        return pdkManager.countAll();
    }

    public Boolean isValidPdk(String pdkId) {
        return pdkManager.isValidpdk(pdkId);
    }

    public void save(PelatihanDasarKeanggotaan pelatihanDasarKeanggotaan) {
        pdkManager.save(pelatihanDasarKeanggotaan);
    }

    public PelatihanDasarKeanggotaan getPDKByRrn(String rrn) {
        return pdkManager.findByRrn(rrn);
    }

    public List<PelatihanDasarKeanggotaan> findIsDeletedPdkList() {
        return pdkManager.findIsDeletedPdkList();
    }

}