package id.co.telkomsigma.btpns.mprospera.manager.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.ManageApkDao;
import id.co.telkomsigma.btpns.mprospera.manager.ManageApkManager;
import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;

@SuppressWarnings("RedundantIfStatement")
@Service("manageApkManager")
public class ManageApkManagerImpl implements ManageApkManager {

    @Autowired
    ManageApkDao manageApkDao;

    @Override
    public List<ManageApk> getAll() {
        return manageApkDao.findAll();
    }

    @Override
    public void clearCache() {
        // TODO Auto-generated method stub
    }

}