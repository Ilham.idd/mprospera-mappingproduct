package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.feign.VisionInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("wsValidationService")
public class WSValidationService extends GenericService {

    @Autowired
    private VisionInterface visionInterface;

    public String wsValidation(String username, String imei, String sessionKey, String apkVersion) {

        log.info("Validating REST Message from Android");
        return visionInterface.wsValidation(username, imei, sessionKey, apkVersion);
    }

}