package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;

public interface PmManager {

    void save(ProjectionMeeting pm);

    Boolean isValidPm(String pmId);

    Page<ProjectionMeeting> getPm();

    Page<ProjectionMeeting> getPmPageable(PageRequest pageRequest);

    Page<ProjectionMeeting> findPmByCreatedDate(Date startDate, Date endDate);

    Page<ProjectionMeeting> findPmByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest);

    Page<ProjectionMeeting> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate);

    Page<ProjectionMeeting> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
                                                      PageRequest pageRequest);

    void clearCache();

    ProjectionMeeting findByRrn(String rrn);

    List<ProjectionMeeting> findByMmId(MiniMeeting mmId);

    ProjectionMeeting findById(String pmId);

    List<ProjectionMeeting> findIsDeletedPmList();

}